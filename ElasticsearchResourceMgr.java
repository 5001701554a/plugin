package com.testOpensearchPlugin;

import java.util.List;
        import java.util.Map;

import org.apache.commons.collections.MapUtils;
        import org.apache.commons.lang.StringUtils;
        import org.apache.log4j.Logger;
        import org.apache.ranger.plugin.service.ResourceLookupContext;

public class ElasticsearchResourceMgr {

    public static final String INDEX = "index";

    private static final Logger LOG = Logger.getLogger(ElasticsearchResourceMgr.class);

    public static Map<String, Object> validateConfig(String serviceName, Map<String, String> configs) throws Exception {
        Map<String, Object> ret = null;

        if (LOG.isDebugEnabled()) {
            LOG.debug("==> com.testOpensearchPlugin.ElasticsearchResourceMgr.validateConfig() serviceName: " + serviceName + ", configs: "
                    + configs);
        }

        try {
            ret = ElasticsearchClient.connectionTest(serviceName, configs);
        } catch (Exception e) {
            LOG.error("<== com.testOpensearchPlugin.ElasticsearchResourceMgr.validateConfig() error: " + e);
            throw e;
        }

        if (LOG.isDebugEnabled()) {
            LOG.debug("<== com.testOpensearchPlugin.ElasticsearchResourceMgr.validateConfig() result: " + ret);
        }
        return ret;
    }

    public static List<String> getElasticsearchResources(String serviceName, Map<String, String> configs,
                                                         ResourceLookupContext context) {
        String userInput = context.getUserInput();
        String resource = context.getResourceName();
        Map<String, List<String>> resourceMap = context.getResources();
        if (LOG.isDebugEnabled()) {
            LOG.debug("==> com.testOpensearchPlugin.ElasticsearchResourceMgr.getElasticsearchResources()  userInput: " + userInput
                    + ", resource: " + resource + ", resourceMap: " + resourceMap);
        }

        if (MapUtils.isEmpty(configs)) {
            LOG.error("Connection config is empty!");
            return null;
        }

        if (StringUtils.isEmpty(userInput)) {
            LOG.warn("User input is empty, set default value : *");
            userInput = "*";
        }

        final ElasticsearchClient elasticsearchClient = ElasticsearchClient.getElasticsearchClient(serviceName, configs);
        if (elasticsearchClient == null) {
            LOG.error("Failed to getElasticsearchResources!");
            return null;
        }

        List<String> resultList = null;

        if (StringUtils.isNotEmpty(resource)) {
            switch (resource) {
                case INDEX:
                    List<String> existingConnectors = resourceMap.get(INDEX);
                    resultList = elasticsearchClient.getIndexList(userInput, existingConnectors);
                    break;
                default:
                    break;
            }
        }

        if (LOG.isDebugEnabled()) {
            LOG.debug("<== com.testOpensearchPlugin.ElasticsearchResourceMgr.getElasticsearchResources() result: " + resultList);
        }
        return resultList;
    }

}
