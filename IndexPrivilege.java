package com.testOpensearchPlugin;

import java.util.List;

public class IndexPrivilege {

    private String privilege;

    private List<String> actions;

    public IndexPrivilege(String privilege, List<String> actions) {
        super();
        this.privilege = privilege;
        this.actions = actions;
    }

    public String getPrivilege() {
        return privilege;
    }

    public void setPrivilege(String privilege) {
        this.privilege = privilege;
    }

    public List<String> getActions() {
        return actions;
    }

    public void setActions(List<String> actions) {
        this.actions = actions;
    }

    @Override
    public String toString() {
        return "com.testOpensearchPlugin.IndexPrivilege [privilege=" + privilege + ", actions=" + actions + "]";
    }
}