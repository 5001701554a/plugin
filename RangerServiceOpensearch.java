package com.testOpensearchPlugin;

import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder;
import org.apache.ranger.plugin.model.RangerPolicy;
import org.apache.ranger.plugin.model.RangerService;
import org.apache.ranger.plugin.model.RangerServiceDef;
import org.apache.ranger.plugin.service.RangerBaseService;
import org.apache.ranger.plugin.service.ResourceLookupContext;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class RangerServiceOpensearch extends RangerBaseService{

    private static final Logger LOG = LoggerFactory.getLogger(RangerServiceOpensearch.class);

    public RangerServiceOpensearch() {
        super();
    }

    @Override
    public void init(RangerServiceDef rangerServiceDef, RangerService rangerService) {
        super.init(rangerServiceDef, rangerService);
    }

    public static void main(String[] args) throws Exception {
        RangerServiceOpensearch r = new RangerServiceOpensearch();
        r.validateConfig();
    }

    @Override
    public Map<String, Object> validateConfig() throws Exception {
        Map<String, Object> ret = new HashMap<>();

//        System.setProperty("javax.net.ssl.trustStore", "/usr/lib/jvm/java-1.8.0-openjdk-amd64/jre/lib/security/cacerts");
//    System.setProperty("javax.net.ssl.trustStorePassword", "changeit");

        //Establish credentials to use basic authentication.
        //Only for demo purposes. Do not specify your credentials in code.
        final CredentialsProvider credentialsProvider = new BasicCredentialsProvider();

        credentialsProvider.setCredentials(AuthScope.ANY,
                new UsernamePasswordCredentials("admin", "admin"));

        //Create a client.
        RestClientBuilder builder = RestClient.builder(new HttpHost("10.129.0.29", 9200, "https"))
                .setHttpClientConfigCallback(new RestClientBuilder.HttpClientConfigCallback() {
                    @Override
                    public HttpAsyncClientBuilder customizeHttpClient(HttpAsyncClientBuilder httpClientBuilder) {
                        return httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider);
                    }
                });



            RestHighLevelClient client = new RestHighLevelClient(builder);
            IndexRequest request = new IndexRequest("custom-index"); //Add a document to the custom-index we created.
            request.id("1"); //Assign an ID to the document.

            HashMap<String, String> stringMapping = new HashMap<String, String>();
            stringMapping.put("message:", "Testing Java REST client");
            request.source(stringMapping); //Place your content into the index's source.
            IndexResponse indexResponse = client.index(request, RequestOptions.DEFAULT);

            //Getting back the document
            GetRequest getRequest = new GetRequest("custom-index", "1");
            GetResponse response = client.get(getRequest, RequestOptions.DEFAULT);

            System.out.println(response.getSourceAsString());


        //TODO: looking to the opensearch config and implements its validations

        return ret;
    }

    @Override
    public List<RangerPolicy> getDefaultRangerPolicies() throws Exception {
        if (LOG.isDebugEnabled()) {
            LOG.debug("==> com.testOpensearchPlugin.RangerServiceOpensearch.getDefaultRangerPolicies() ");
        }

        List<RangerPolicy> ret = super.getDefaultRangerPolicies();
        //TODO: here we must to decide what we need to do with policies like:
//        ret = ElasticsearchResourceMgr.getElasticsearchResources(serviceName, configs, context);


        if (LOG.isDebugEnabled()) {
            LOG.debug("<== com.testOpensearchPlugin.RangerServiceOpensearch.getDefaultRangerPolicies(): " + ret);
        }

        return ret;
    }

    @Override
    public List<String> lookupResource(ResourceLookupContext context) throws Exception {

        List<String> ret = new ArrayList<String>();
        Map<String, String> configs = getConfigs();
        if (context != null) {
            try {
                //TODO: check in Opensearch
//                ret = ElasticsearchResourceMgr.getOpenSearchResources(serviceName, configs, context);
            } catch (Exception e) {
                LOG.info("com.testOpensearchPlugin.RangerServiceOpensearch.lookupResource error: " + e);
            }
        }
        return ret;
    }
}
